const hbs = require('hbs')

hbs.registerHelper('getAnio', () => {
    return new Date().getUTCFullYear();
})

hbs.registerHelper('capitalizar', (texto) => {
    let palabras = texto.split(" ");
    palabras.forEach((item, idx) => {
        palabras[idx] = item.charAt(0).toUpperCase() + item.slice(1);
    });

    return palabras.join(' ');
})

